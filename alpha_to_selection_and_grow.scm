(define (alpha-to-selection-and-grow-create-layer image drawable)

  ; Get the name of the current layer
  (let* ((layer-name (car (gimp-item-get-name drawable)))
         ; Append " Noise" to the layer name
         (new-layer-name (string-append layer-name " Noise"))
         ; Create a new layer with the new name
         (new-layer (car (gimp-layer-new image
                                         (car (gimp-image-width image))
                                         (car (gimp-image-height image))
                                         RGBA-IMAGE
                                         new-layer-name
                                         100
                                         NORMAL-MODE))))
    ; Add the new layer to the image
    (gimp-image-insert-layer image new-layer 0 -1)
    ; Convert alpha to selection
    (gimp-selection-layer-alpha drawable)
    ; Apply Solid Noise
    (plug-in-solid-noise 1 image new-layer FALSE FALSE 4576 15 1 16)
    ; Set Layer Mode to Dodge
    (gimp-layer-set-mode new-layer 42)
  )

  (gimp-selection-none image)
 
  (gimp-displays-flush)

)

(script-fu-register
  "alpha-to-selection-and-grow-create-layer"
  "Alpha to Selection, Grow and Create Layer"
  "Convert alpha to selection, grow the selection by 2 pixels, and create a new layer with the name of the current layer and the word \"Rand\" appended, fill the selection with black, and put the layer behind the current selected layer"
  "Author Name"
  "Author Name"
  "Date"
  ""
  SF-IMAGE "Image" 0
  SF-DRAWABLE "Drawable" 0
)

(script-fu-menu-register "alpha-to-selection-and-grow-create-layer"
                         "<Image>/Filters/Alpha to Selection, Grow and Create Layer")