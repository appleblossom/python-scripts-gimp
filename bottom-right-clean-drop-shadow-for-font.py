#!/usr/bin/env python

from gimpfu import *

def solid_shadow(image, drawable):

    # Gaussian Blur
    # Get the name of the current layer
    layer_name = pdb.gimp_item_get_name(drawable)
    # Append " Bottom Right Hintergrund" to the layer name
    new_layer_name = layer_name + " Bottom Right Solid Shadow"
    # Create a new layer with the new name
    new_layer = pdb.gimp_layer_new(image, image.width, image.height, RGBA_IMAGE, new_layer_name, 100, NORMAL_MODE)
    # Add the new layer to the image
    pdb.gimp_image_insert_layer(image, new_layer, None, 1)
    # Convert alpha to selection
    pdb.gimp_selection_layer_alpha(drawable)
    # Grow the selection by five pixels
    pdb.gimp_selection_grow(image, 5)
    # Fill the selection with black
    pdb.gimp_edit_bucket_fill(new_layer, 0, 0, 100, 255, FALSE, 0, 0)
    # Clear selection
    pdb.gimp_selection_none(image)
    # Apply Gaussian blur with 20 pixels
    pdb.plug_in_gauss(image, new_layer, 50, 50, 0)

    # Bottom Right
    # Get the name of the current layer
    layer_name = pdb.gimp_item_get_name(drawable)
    # Append " Bottom Right Hintergrund" to the layer name
    new_layer_name = layer_name + " Bottom Right"
    # Create a new layer with the new name
    new_layer = pdb.gimp_layer_new(image, image.width, image.height, RGBA_IMAGE, new_layer_name, 100, NORMAL_MODE)
    # Add the new layer to the image
    pdb.gimp_image_insert_layer(image, new_layer, None, 1)
    # Convert alpha to selection
    pdb.gimp_selection_layer_alpha(drawable)
    # Fill the selection with black
    pdb.gimp_edit_bucket_fill(new_layer, 0, 0, 100, 255, FALSE, 0, 0)
    # Clear selection
    pdb.gimp_selection_none(image)
    # Move the layer up and to the left by 5 pixels
    # First Number positive right
    # First Number negative left
    # Second Number positive bottom
    # Second Number negative top
    pdb.gimp_layer_set_offsets(new_layer, 9, 9)

    pdb.gimp_displays_flush()

    # Set the selection to the original layer
    pdb.gimp_image_set_active_layer(image, drawable)

    # Call the "alpha_to_selection_and_grow_create_layer" GIMP Fu script
    pdb.alpha_to_selection_and_grow_create_layer(image, drawable)

register(
    "bottom-right-clean-drop-shadow-for-font",
    "Font Edit Python",
    "Prints 'Hello, world!' to the GIMP message console.",
    "Author Name",
    "Author Name",
    "2023",
    "<Image>/Filters/Font Python/Bottom Right Clean Drop Shadow For Font",
    "",
    [],
    [],
    solid_shadow)

main()